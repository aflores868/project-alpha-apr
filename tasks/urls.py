from django.urls import path
from . import views
from .views import my_tasks

urlpatterns = [
    path("mine/", my_tasks, name="show_my_tasks"),
    path("create/", views.create_task, name="create_task"),
]
