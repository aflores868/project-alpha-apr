from django.shortcuts import render, get_object_or_404, redirect
from .models import Project, ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here
@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/project_list.html", {"object_list": projects}
    )


@login_required
def project_detail_view(request, pk):
    project = get_object_or_404(Project, pk=pk)
    return render(request, "projects/project_detail.html", {"object": project})


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "create_project.html", {"form": form})
