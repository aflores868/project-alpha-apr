from django.urls import path
from .views import project_list, project_detail_view, create_project

urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:pk>/", project_detail_view, name="show_project"),
    path("", project_list, name="list_projects"),
]
